terraform {
  required_providers {
    local = {
      source  = "hashicorp/local"
      version = "~> 1.4"
    }
  }
}

# Terraform backend S3
terraform {
  backend "s3" {
    # Provide key and dynamodb_table as backend configuration
    # bucket = "terraform-bucket-demo"
    # key    = "eks-cluster1/terraform.tfstate"
    # region = "us-east-1"
    # dynamodb_table = "terraform_state-dynamodb"
  }
}

# Terraform backend GitLab-managed Terraform state
# terraform {
#   backend "http" {
#   }
# }
# Maven web application project

### Clean fresh application code is available in hello-world-maven branch
To clone the hello-world-maven branch
```
git clone -b hello-world-maven https://github.com/vigneshsweekaran/hello-world.git
```
### To generate the package
```
mvn clean package
```
### War file is generated in target/hello-world.war

## Gitlab ci/cd Trainings
|Topic|Branch name| 
|----|-----|
|First pipeline|first-pipeline|
|variables in Pipeline and Job|variables|
|How to define docker image in Pipeline and Job|image|
|How to build a maven java project|maven-build|
|How to use Artifacts in a pipeline or Job|artifacts-job, artifacts-default|
|How to use cache feature in Job|cache-job, cache-default|
|What is services in gitlab cicd|services-job, services-default|
|How to build a docker image using service container|maven-docker-build-dind-failure|
|How to build a docker image using service container|maven-docker-build-dind|
|How to use before_script and after_script in job|before-after-script-job|
|How to use before_script and after_script in default section |before-after-script-default|
|How to build a docker image and push to Dockerhub|maven-docker-build-dind-dockerhub|
|How to build a docker image and push to jfrog|maven-docker-build-dind-artifactory|
|Use of only and except in pipelien and job|only-except|
|Use of rules in pipeline and Job|rules|
|Use of workflows in pipeline|workflows|
|How to deploy the container to a server|maven-build-deploy|
|How to setup a private runner with docker executor|maven-docker-build|
|How to pass variables from one job to another job|variables-to-another-job|
|Internal template|internal-template|
|Template from same repo|template-same-repo|
|Template from different repo|template-different-repo|
|Integrating gitlab cicd container scanning template|container-scanning-template|
|Deploy to kubernetes using kubectl using ssh|deploy-k8s-kubectl-ssh|
|Deploy to kubernetes using kubectl from build container itself|deploy-k8s-kubectl-build-container|
|Build helm chart from piepline and push to jfrog helm repository|helm-jfrog|
|Deploy to kubernetes using helm using ssh|deploy-k8s-helm-ssh|
|Deploy to kubernetes using helm from build container itself|deploy-k8s-helm-build-container|
|How to upgrade the deployment using helm|k8s-helm-upgrade|
|Use of resource_group in Job|resource-group|
|Use of environment in Job|environment|
|How to create varibale with different values for diffrent environment|varibales-different-environment|
|How to mark a job as success, even it fails|allow-failure|
|How to retry the whole job when it fails|retry|
|How to send a mail using pipeline|mail|
|How to adopt your pipeline to run on both docker and k8s executor|docker-k8s-executor|
|How to capture the failure in script section and handle it accordingly||


### Terraform Commands
#### Terraform Init
```
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/30252780/terraform/state/hello-world" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/30252780/terraform/state/hello-world/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/30252780/terraform/state/hello-world/lock" \
    -backend-config="username=vigneshsweekaran" \
    -backend-config="password=<YOUR-ACCESS-TOKEN>" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

##### Terraform plan
```
terraform plan
```

##### Terraform apply
```
terraform apply -auto-approve
```
